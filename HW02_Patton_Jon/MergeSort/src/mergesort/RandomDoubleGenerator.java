/* Jon S. Patton
 * COSC 336
 * Dr. Nguyen
 * March 21, 2018
 * Merge sort problem random number generator.
 */

package mergesort;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Random;

public class RandomDoubleGenerator {
    
    //private int integers[];
    private double doubles[];
    private Random rand;
    
    //Generates just an array.
    public RandomDoubleGenerator(int numberOfValues)
    {
        rand = new Random();
        doubles = new double[numberOfValues];
        
        for (int i = 0; i < numberOfValues; i++)
        {
            doubles[i] = rand.nextDouble();
        }
    }
    
    //Generates a file with random numbers.
    public RandomDoubleGenerator(String fileName, int numberOfValues) throws IOException
    {
        rand = new Random();
        PrintStream ps = new PrintStream(new File(fileName));
        
        for (int i = 0; i < numberOfValues; i++)
        {
            ps.print(rand.nextDouble() + " ");
        }
    }
    
}
