/* Jon S. Patton
 * COSC 336
 * Dr. Nguyen
 * March 21, 2018
 * Merge sort problem - the actual, well, sorter.
 */

package mergesort;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class MergeSort {
    private int mergeType;

    /**
     * When the object is created, a particular type of sorting method will be set.
     * @param mergeType 
     */
    public MergeSort(int mergeType)
    {
        this.mergeType = mergeType;
    }
    
    /**
     * Sort based on the preference at object creation.
     * @param A 
     */
    public void sort(double A[])
    {
        if (mergeType == 1)
            this.mergeSort_recursive(A, 0, A.length - 1);
        else if (mergeType == 2)
            this.mergeSort_iterative(A);
        else if (mergeType == 3)
            this.mergeSort_recursive_insertion(A, 0, A.length - 1);
        else if (mergeType == 4)
            this.mergeSort_iterative_insert(A);
    }
    
    /**
     * Accepts an double array and upper and lower bounds of the array.
     * Adapted from Skiena's implementation in C from "The Algorithm Design Manual"
     * though it's fairly standard.
     * @param s
     * @param low
     * @param high 
     */
    public void mergeSort_recursive(double A[], int low, int high)
    {
        int middle;                                     //middle element
    
        if (low < high)
        {
            middle = (low + high)/2;                    //Split
            mergeSort_recursive(A, low, middle);        //Recurse on the lower half
            mergeSort_recursive(A, middle + 1, high);   //recurse on the higher half
            merge(A, low, middle, high);                //Magic.
        }
    }
    
    //A lot of the time saved here is actually the overhead of the queues, which are
    //still better than intializing arrays in Java.
    public void mergeSort_recursive_insertion(double A[], int low, int high)
    {
        if (A.length < 25)
        {
            insertionSort(A);
            return;
        }
        
        int middle;                                     //middle element
    
        if (low < high)
        {
            middle = (low + high)/2;                    //Split
            mergeSort_recursive_insertion(A, low, middle);        //Recurse on the lower half
            mergeSort_recursive_insertion(A, middle + 1, high);   //recurse on the higher half
            merge(A, low, middle, high);                //Magic.
        }
        return;
    }
    
    /**
     * Merges two portions of an array.
     * Creates a queue to temporarily hold values to avoid overwriting. This may
     * be slightly better than using arrays because Java would initialize
     * the array values anyway, and there is less for the garbage collection to clean
     * up, so even though there's overhead with an interface and linked list,
     * this could actually be a little faster.
     * @param s
     * @param low
     * @param middle
     * @param high 
     */    
    public void merge(double A[], int low, int middle, int high)
    {
        int q;                                            //Counter
        Queue<Double> buffer1 = new LinkedList<Double>(); //Temporary queues.
        Queue<Double> buffer2 = new LinkedList<Double>();
        
        //Fill the buffers.
        for (q = low; q <= middle; q++)
        {
            buffer1.add(A[q]);
        }
        for (q = middle + 1; q <= high; q++)
        {
            buffer2.add(A[q]);
        }
        
        q = low;
        
        //Merge via dequeing and comparing.
        while (!(buffer1.isEmpty()) && !(buffer2.isEmpty()))
        {
            if (buffer1.peek() <= buffer2.peek())
            {
                A[q++] = buffer1.remove();
            }
            else
            {
                A[q++] = buffer2.remove();
            }
        }
        
        //Clean up leftovers.
        while (!buffer1.isEmpty())
        {
            A[q++] = buffer1.remove();
        }
        while (!buffer2.isEmpty())
        {
            A[q++] = buffer2.remove();
        }
    }

    /**
     * Iterative, bottom-up merge sort with queues. Adapted partly from the wikipedia description.
     * https://en.wikipedia.org/wiki/Merge_sort
     * Well ... sort of. The merge function there didn't work properly (it left
     * the last value in the array as 0 and skipped another value somewhere), 
     * and many other versions assume an array at least close to a power of 2,
     * but the method show in 
     * http://andreinc.net/2010/12/26/bottom-up-merge-sort-non-recursive/ *does* work,
     * though I changed it to use queues instead (and kept some other parts from the wiki version).
     * This uses no external function calls at all.
     * @param A 
     */
    public void mergeSort_iterative(double[] A)
    {
	int iLeft, iRight, q; //Bounding variables
        int n = A.length;
        Queue<Double> right = new LinkedList<Double>(); //Temporary queues.
        Queue<Double> left = new LinkedList<Double>();
 
	for (int i = 1; i < n; i *= 2)
        {
            iLeft = 0;
            iRight = i;
            
            while(iRight + i <= n)
            {
                for(q = iRight; q < iRight + i; q++)
                {
                    right.add(A[q]);
                        
		}
		for(q = iLeft; q < iLeft + i; q++)
                {
                    left.add(A[q]);
		}
 
                //Merge via dequeing and comparing.
                q = iLeft;
                while (!(right.isEmpty()) && !(left.isEmpty()))
                {
                    if (right.peek() <= left.peek())
                    {
                        A[q++] = right.remove();
                    }
                    else
                    {
                        A[q++] = left.remove();
                    }
                }

                //Clean up leftovers.
                while (!right.isEmpty())
                {
                    A[q++] = right.remove();
                }
                while (!left.isEmpty())
                {
                    A[q++] = left.remove();
                }
                
                iLeft = iRight + i;
                iRight = iLeft + i;
            }

            if(iRight < n) 
            {
                for(q = iRight; q < n; q++)
                {
                    right.add(A[q]);
                        
		}
		for(q = iLeft; q < iLeft + i; q++)
                {
                    left.add(A[q]);
		}
 
                //Merge via dequeing and comparing.
                q = iLeft;
                while (!(right.isEmpty()) && !(left.isEmpty()))
                {
                    if (right.peek() <= left.peek())
                    {
                        A[q++] = right.remove();
                    }
                    else
                    {
                        A[q++] = left.remove();
                    }
                }

                //Clean up leftovers.
                while (!right.isEmpty())
                {
                    A[q++] = right.remove();
                }
                while (!left.isEmpty())
                {
                    A[q++] = left.remove();
                }
            }
        }
    }
    
    /**
     * Iterative, bottom-up merge sort with queues and interstion sort subroutine.
     * @param A 
     */
    //So what happens normally? We'd start with 2-item arrays and put them in order
    //right away. Then the next iteration we can be pretty sure that the two arrays
    //are fairly balanced.
    //Here, we get to start with larger "chuncks" but that's about it.
    //The output data indicates that this sometimes works well, but we're still
    //n^2 sorting on most of the entire array -- just small chunks of it at a time.
    //So I'm not surprised to see that sometimes it's worse than standard bottom-up.
    public void mergeSort_iterative_insert(double A[])
    {   
        //We need a working array, which involves copying the entire thing.
        //This means that we need double memory at a minimum, though the lack
        //of extra stack frames probably balances it out.
        //double[] B = Arrays.copyOf(A, A.length);
        
        int iLeft, iRight, q; //Bounding variables
        int n = A.length;
        Queue<Double> right = new LinkedList<Double>(); //Temporary queues.
        Queue<Double> left = new LinkedList<Double>();
        
        //The size of the arrays to do insertion sort on.
        int subArraySize = 25;
        
        if (A.length < subArraySize)
        {
            insertionSort(A);
            return;
        }
        
        //Base case: Sort chuncks of the big array with insertion sort.
        q = 0;
        while (q < n)
        {
            int i = q + 1;
            int count = 0;
            int j;
            double x;
        
            int length = A.length;
        
            while (i < length && count < 25)
            {
                x = A[i];
                j = i - 1;
                while ((j >= q) && (A[j] > x))
                {
                    A[j+1]  = A[j];
                    j--;
                }
                A[j+1] = x;
                i++;
                count++;
            }
            q+= subArraySize;
        }
         
        //Merge sort chunks bigger than the subArraySize.
	for (int i = subArraySize; i < n; i *= 2)
        {
            iLeft = 0;
            iRight = i;
            
            while(iRight + i <= n)
            {
                for(q = iRight; q < iRight + i; q++)
                {
                    right.add(A[q]);
                        
		}
		for(q = iLeft; q < iLeft + i; q++)
                {
                    left.add(A[q]);
		}
 
                //Merge via dequeing and comparing.
                q = iLeft;
                while (!(right.isEmpty()) && !(left.isEmpty()))
                {
                    if (right.peek() <= left.peek())
                    {
                        A[q++] = right.remove();
                    }
                    else
                    {
                        A[q++] = left.remove();
                    }
                }

                //Clean up leftovers.
                while (!right.isEmpty())
                {
                    A[q++] = right.remove();
                }
                while (!left.isEmpty())
                {
                    A[q++] = left.remove();
                }
                
                iLeft = iRight + i;
                iRight = iLeft + i;
            }

            if(iRight < n) 
            {
                for(q = iRight; q < n; q++)
                {
                    right.add(A[q]);
                        
		}
		for(q = iLeft; q < iLeft + i; q++)
                {
                    left.add(A[q]);
		}
 
                //Merge via dequeing and comparing.
                q = iLeft;
                while (!(right.isEmpty()) && !(left.isEmpty()))
                {
                    if (right.peek() <= left.peek())
                    {
                        A[q++] = right.remove();
                    }
                    else
                    {
                        A[q++] = left.remove();
                    }
                }

                //Clean up leftovers.
                while (!right.isEmpty())
                {
                    A[q++] = right.remove();
                }
                while (!left.isEmpty())
                {
                    A[q++] = left.remove();
                }
            }
        }
    }
    
    /**
     * Bog standard iterative insertion sort.
     * @param A
     */
    public void insertionSort(double A[])
    {
        int i = 1;
        int j;
        double x;
        
        int length = A.length;
        
        while (i < length)
        {
            x = A[i];
            j = i - 1;
            while ((j >= 0) && (A[j] > x))
            {
                A[j+1]  = A[j];
                j--;
            }
            A[j+1] = x;
            i++;
        }
    }    

}
