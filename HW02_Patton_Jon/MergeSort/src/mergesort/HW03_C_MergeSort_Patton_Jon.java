/* Jon S. Patton
 * COSC 336
 * Dr. Nguyen
 * March 21, 2018
 * Merge sort problem test driver.
 */
package mergesort;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

public class HW03_C_MergeSort_Patton_Jon {
    public static void main(String[] args)
    {
        MergeSort[] sorts = new MergeSort[4];
        
        File f;
        RandomDoubleGenerator rdg;
        double d[] = {};
        Scanner s;
        PrintStream ps;
        
        int loops = 5; //the number of different ranges of numbers we want.
        
        long[] data = new long[7*loops];
        long time;
        int t = 0;
        int k;
        int base = (int) Math.pow(10, 6); //how big do ya wanna go?
        int jump = (int) (5 * Math.pow(10, 5));
        
        try
        {
            for (int i = 0; i < loops; i++)
            {
                //Start timing.
                //data[i + t] = 0;
                //t++;
                time = System.nanoTime();
                
                int N = base + (i * jump);
                rdg = new RandomDoubleGenerator("inputHW02.txt", N);
                
                //Time take to generate N numbers to a file.
                data[i + t] = System.nanoTime() - time;
                time = System.nanoTime();
                t++;
                
                f = new File("inputHW02.txt");
                
                d = new double[N];
                s = new Scanner(f);

                k = 0;
                while (s.hasNextDouble())
                {
                    d[k++] = s.nextDouble();
                }
                //Time taken to read N numbers from file.
                //Only want to time this once.
                data[i + t] = System.nanoTime() - time;
                time = System.nanoTime();
                t++;
                
                for (int j = 1; j <= 4; j++)
                {   
                    sorts[j - 1] = new MergeSort(j);
                    sorts[j - 1].sort(d);
                    //time to sort with the given method.
                    data[i + t] = System.nanoTime() - time;
                    
                    if (i < 4)
                    {
                        d = new double[N];
                    
                        k = 0;
                        while (s.hasNextDouble())
                        {
                            d[k++] = s.nextDouble();
                        }
                    }
                    time = System.nanoTime();
                    t++;
                }
                ps = new PrintStream(new File("outputHW02-" + i + ".txt"));
                for (int j = 0; j < N; j++) ps.print(d[j] + " ");
                
                //Time taken to write to file.
                data[i + t] = System.nanoTime() - time;
                //time = System.nanoTime();
                //t++;
                
            }
        }
        catch(IOException e)
        {
            System.err.println("Error: file read/write.");
        }
        try
        {
            ps = new PrintStream(new File("TimingData.txt"));
            ps.print("Numbers (N), generate N to file, Read N from file, Sort recursively, Sort recursively with subroutine, Sort iteratively, Sort iteratively with subroutine, Write sorted N to file\n");
            for (int i = 0; i < data.length; i += 7)
            {
                ps.print((base + (i * jump)/7) + ",");
                for (int j = 0; j < 7; j++)
                    ps.print(data[j + i] + ",");
                ps.print("\n");
            }
            
        }
        catch(IOException e)
        {
            System.err.println("Error: file read/write.");
        }
    }
    
}
